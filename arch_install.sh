#!/bin/bash
#
# The purpose of this script is to automate the installation of Arch Linux a little bit.
# The Information for writing this script was taken from the Arch Wiki, especially from
# following sites:
# https://wiki.archlinux.org/index.php/Installation_guide
# https://wiki.archlinux.org/index.php/beginners'_guide
# https://wiki.archlinux.org/index.php/GRUB 
#
# For this script to work partitioning must be done before
# (e.g. via a live system and using gparted)
# and then the config (dev_mount_map, BOOT_DEVICE, SWAP_DEV) must be changed accordingly
# As boot loader currently only grub is supported (see BOOT_LOADER)
#
# I would recommend to run the script for example like
# &> ./arch_install &> out.txt
# in order to be able to look at the output of the whole procedure to see if everything
# went well.
#
# For better readability in Notepad++ change under Settings > Style Configurator
# under Language: bash the Style for HERE Q 
#

# ####################################################################################
#   CHANGE THE CONFIG AS APPROPRIATE
# ####################################################################################

COMPUTER_NAME="myHost"
# devices - mount point mapping
declare -A dev_mount_map
dev_mount_map+=(
 ["/dev/sda1"]=/mnt
 ["/dev/sdb1"]=/mnt/home
)
BOOT_DEVICE="/dev/sda"
SWAP_DEV="/dev/sda2"
BOOT_LOADER="grub"
SET_LOCALE="de_AT.UTF-8"
KEY_MAP="de-latin1-nodeadkeys"
TIME_ZONE="Europe"
TIME_SUBZONE="Vienna"
LOCALES_ARR=( "de_AT.UTF-8 UTF-8" "de_AT ISO-8859-1" "de_AT@euro ISO-8859-15" )

# ####################################################################################
#   DO NOT EDIT ABOVE THIS LINE
# ####################################################################################

loadkeys $KEY_MAP # german keyboardlayout

# check inet connection
if [[ -n "$(ping -c3 www.google.com 2>&1 | grep "unknown host")" ]]; then
    echo "Error: No network connection"
    echo "Set up a network connection and then try again..."
    exit 1
fi

# update system clock
timedatectl set-ntp true 
timedatectl status

# Mount the partitions
# e.g.  mount /dev/foo /dir
#       mount /dev/foo /dir -o defaults,noatime
for key in ${!dev_mount_map[@]}; do
    mkdir ${dev_mount_map[${key}]} # note: let it fail it dir already exits
    mount ${key} ${dev_mount_map[${key}]}
done

# set up and activate swap
swapon $SWAP_DEV

# install the base packages
pacstrap /mnt base base-devel

# generate the fstab file (use -U or -L to define by UUID or labels)
genfstab -p /mnt > /mnt/etc/fstab

arch-chroot /mnt /bin/bash -i <<EOF
    # set hostname
    echo $COMPUTER_NAME > /etc/hostname
    # set timezone
    ln -s /usr/share/zoneinfo/$TIME_ZONE/$TIME_SUBZONE /etc/localtime
    # set hardware clock to UTC
    # see https://wiki.archlinux.org/index.php/beginners'_guide#Time
    hwclock --systohc --utc
    # generate locales
    # XXX hack - because loop didn't work
    printf "#\n%s %s\n" "${LOCALES_ARR[0]}" >> /etc/locale.gen
    printf "%s %s\n" "${LOCALES_ARR[1]}" >> /etc/locale.gen
    printf "%s %s\n" "${LOCALES_ARR[2]}" >> /etc/locale.gen
    locale-gen
    # set locale preferences
    echo LANG=$SET_LOCALE > /etc/locale.conf
    # set keymap
    # add 'FONT=<font>' to set a font
    # see https://wiki.archlinux.org/index.php/Fonts#Console_fonts
    echo KEYMAP=$KEY_MAP > /etc/vconsole.conf
    # create new initial RAM disk
    mkinitcpio -p linux    
    # install a boot loader
    # e.g. GRUB
    if [[ "$BOOT_LOADER" == "grub" ]]; then
        echo "Installing grub as boot loader..."
        pacman -S --noconfirm grub
        # the following commands will:
        # + Set up GRUB in the 440-byte Master Boot Record boot code region
        # + Populate the /boot/grub directory
        # + Generate the /boot/grub/i386-pc/core.img file
        # + Embed it in the 31 KiB (minimum size - varies depending on partition alignment) 
        #     post-MBR gap in case of MBR partitioned disk
        # + In the case of a GPT partitioned disk it will embed it in the BIOS Boot Partition , 
        #     denoted by bios_grub flag in parted and EF02 type code in gdisk
        grub-install --recheck --target=i386-pc $BOOT_DEVICE
        grub-mkconfig -o /boot/grub/grub.cfg
    fi
EOF

# umount -R /mnt
# reboot









