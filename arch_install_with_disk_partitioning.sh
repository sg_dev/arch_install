#!/bin/bash
#
# This is an experimental version of the install script using fdisk for partitioning
# (so no partitioning before executing this script needed)
# However, currently this procedure is not very flexible. 
#

COMPUTER_NAME="myHost"
# devices - mount point mapping
declare -A dev_mount_map
dev_mount_map+=(
 ["/dev/sda1"]=/mnt
 ["/dev/sda2"]=/mnt/home
)
BOOT_DEVICE="/dev/sda"
SWAP_DEV="/dev/sda3"
BOOT_LOADER="grub"
SET_LOCALE="de_AT.UTF-8"
KEY_MAP="de-latin1-nodeadkeys"
TIME_ZONE="Europe"
TIME_SUBZONE="Vienna"
LOCALES_ARR=( 'de_AT.UTF-8 UTF-8' 'de_AT ISO-8859-1' 'de_AT@euro ISO-8859-15' )

loadkeys $KEY_MAP # german keyboardlayout

# check inet connection
if [[ -n "$(ping -c3 www.google.com | grep -q "unknown host")" ]]; then
    echo "Error: No network connection"
    echo "Set up a network connection and then try again..."
    exit 1
fi

# update system clock
timedatectl set-ntp true 
timedatectl status

# partition the disk
# to create the partitions programatically (rather than manually)
# we're going to simulate the manual input to fdisk
# The sed script strips off all the comments so that we can 
# document what we're doing in-line with the actual commands
# Note that a blank line (commented as "defualt" will send a empty
# line terminated with a newline to take the fdisk default.
#
# create swap: n, (p?), <leave blank-> default value>, <partsize, e.g. +4GB>, t, <partition-nr>, 82 
# http://computernetworkingnotes.com/file-system-administration/how-to-create-swap-partition.html
#
sed -e 's/    \([\+0-9a-zA-Z]*\)[ ].*/\1/' << EOF | fdisk ${BOOT_DEVICE}
    o   # clear the in memory partition table
    n   # new partition - root
    p   # primary partition
    1   # partition number 1
        # default - start at beginning of disk 
    +7GB    # 7GB / partition
    n    # new partition (swap)
    p    # primary
    3    # partition nr 3
        # default
    +4GB #
    t    # change type of ..
    3    # ... part. nr. 3 to ...
    82    # ... swap
    n   # new partition - home
    p   # primary partition - home partition
    2   # partion number 2
        # default, start immediately after preceding partition
        # default, extend partition to end of disk
    a   # make a partition bootable
    1   # bootable partition is partition 1 -- /dev/sda1
    p   # print the in-memory partition table
    w   # write the partition table
    q   # and we're done
EOF

# re-read partition table
# partprobe

# Mount the partitions
# e.g.  mount /dev/foo /dir
#       mount /dev/foo /dir -o defaults,noatime
for key in ${!dev_mount_map[@]}; do
    # make filesystem
    mkfs.ext4 -q ${key}
    mkdir ${dev_mount_map[${key}]} # note: let it fail it dir already exits
    mount ${key} ${dev_mount_map[${key}]}
done

# set up and activate swap
mkswap $SWAP_DEV
swapon $SWAP_DEV

# install the base packages
pacstrap /mnt base base-devel

# generate the fstab file (use -U or -L to define by UUID or labels)
genfstab -p /mnt > /mnt/etc/fstab

arch-chroot /mnt /bin/bash -i <<EOF
    # set hostname
    echo $COMPUTER_NAME > /etc/hostname
    # set timezone
    ln -s /usr/share/zoneinfo/$TIME_ZONE/$TIME_SUBZONE /etc/localtime
    # set hardware clock to UTC
    # see https://wiki.archlinux.org/index.php/beginners'_guide#Time
    hwclock --systohc --utc
    # generate locales
    for i in "${LOCALES_ARR[@]}"
    do
        printf '%s\n' $i >> /etc/locale.gen
    done
    locale-gen
    # set locale preferences
    echo LANG=$SET_LOCALE > /etc/locale.conf
    # set keymap
    # add 'FONT=<font>' to set a font
    # see https://wiki.archlinux.org/index.php/Fonts#Console_fonts
    echo KEYMAP=$KEY_MAP > /etc/vconsole.conf
    # create new initial RAM disk
    mkinitcpio -p linux    
    # install a boot loader
    # e.g. GRUB
    if [[ "$BOOT_LOADER" == "grub" ]]; then
        echo "Installing grub as boot loader..."
        pacman -S --noconfirm grub
        # the following commands will:
        # + Set up GRUB in the 440-byte Master Boot Record boot code region
        # + Populate the /boot/grub directory
        # + Generate the /boot/grub/i386-pc/core.img file
        # + Embed it in the 31 KiB (minimum size - varies depending on partition alignment) 
        #     post-MBR gap in case of MBR partitioned disk
        # + In the case of a GPT partitioned disk it will embed it in the BIOS Boot Partition , 
        #     denoted by bios_grub flag in parted and EF02 type code in gdisk
        grub-install --recheck --target=i386-pc $BOOT_DEVICE
        grub-mkconfig -o /boot/grub/grub.cfg
    fi
EOF

# commented out below to see if everything worked
# umount -R /mnt
# reboot









