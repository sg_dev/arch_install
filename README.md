# README #

This is a short shell script which can be used to install Arch Linux.
I basically used the information found in https://wiki.archlinux.org/index.php/Installation_guide
to write this script.

## Features ##

* Installs a basic Arch Linux system
* Short config section to change for example your locale or the mount points

## How to run it? ##

* Just execute in the home directory of the installation media:
```
#!shell

./arch_install.sh &> out.txt

```
In this way you can check after the procedure is finished if there were any errors.


## Configuration ##

* All values in the section "CHANGE THE CONFIG AS APPROPRIATE" can be changed according to one's needs.

### Configuration Values ###

* COMPUTER_NAME ... set your hostname
* dev_mount_map ... fill in all the mount points (Note: The partitions MUST be created in advanced)
* BOOT_DEVICE ... the device from which you want to boot (where the boot loader will be written)
* SWAP_DEV ... the partition of your swap partition
* BOOT_LOADER ... the name of the boot loader which will be installed (currently only grub is supported)
* SET_LOCALE ... your preferred locale
* KEY_MAP ... the keyboard layout which you want to use (Note: if you want to use X, you must configure your keyboard layout separately)
* TIME_ZONE/ SUB_ZONE ... time zone information (refers to /usr/share/zoneinfo/$TIME_ZONE/$TIME_SUBZONE)
* LOCALES_ARR ... values which are used by *locale-gen*

## Limitations ##

* Currently only grub is supported as boot loader
* Only tested/used with BIOS/MBR partition layout (don't know if f.e. UEFI/GPT works)
* Partitioning must be done in advance (e.g. via GParted, fdisk, etc.) an the partition must then be modified accordingly. (*arch_install_with_disk_partitioning.sh* contains some experimental version with partitioning using fdisk. However, this procedure is currently not very flexible and (in my opinion) it's much more easier to do the partitioning manually f.e. with a graphical tool like GParted)
* Further stuff like X environment are not installed by the script

## Who do I talk to? ##

* Repo owner Stefan Gschiel (stefan.gschiel.sg@gmail.com)